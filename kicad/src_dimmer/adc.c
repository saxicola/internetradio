/**
 * ADC routines for the atmega8
 *
 * */


#include "adc.h"
#include <util/delay.h>


/*******************************************************************
 *
 * @param char Channel number
 * @return void
 *
 *******************************************************************/
void ADC_init(uint8_t channel)
{
    ADC_DISABLE;
    ADCSRA = 0x00; //disable ADC
    ADMUX = 0x00; //Clear everything
    ADMUX |= channel;// 0x40; //select adc input channel
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1)| (1 << ADPS0);
    ADC_ENABLE;
    _delay_ms(5);
    }

/*******************************************************************
 * @param char Channel number
 * @return int ADC value 0-1024
 *
 *******************************************************************/
uint16_t ADC_read(uint8_t channel)
{
    uint8_t i;
    uint64_t ADC_acc = 0; // Accumulates results.
    uint8_t samples = 8;

    // Disable digital input on the sampling pin.
    switch (channel)
    {
        case 0:
            DIDR0 |= _BV(ADC0D);
            break;
        case 1:
            DIDR0 |= _BV(ADC1D);
            break;
        case 2:
            DIDR0 |= _BV(ADC2D);
            break;
        case 3:
            DIDR0 |= _BV(ADC3D);
            break;
    }

    ADC_init(channel);
    ADC_START_CONVERSION; //do a dummy readout first
    while(!(ADCSRA & 0x10)); // wait for conversion done, ADIF flag active
    ADCSRA |= _BV(ADIF); // Write 1 to clear the flag.

    for(i = 0; i < samples; i++) // do the ADC conversion 8 times for better accuracy
    {
        ADC_START_CONVERSION;
        while(ADCSRA & _BV(ADSC));// wait for conversion to complete
        //_delay_ms(10);
        ADC_acc += ADC;             // accumulate result (8 samples) for later averaging
        _delay_ms(1);
    }
    ADC_DISABLE;
    DIDR0 = 00;
    return (uint16_t)(ADC_acc / samples);//(ADC_acc >> 3); // average the 8 samples


}

