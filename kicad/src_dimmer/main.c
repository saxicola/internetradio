/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */
 /*
  * I2C controlled PWM to provide dimming to numitrons. Or whatever.
  */

 /*
  * To initiate a read, write 1 to REG_DIGI, wait until it's set back to 0 then read the value.
  * The registers.
  #define REG_CTRL    i2c_reg[0]
  #define REG_B       i2c_reg[1]
  #define AZIZ_0      i2c_reg[2]
  #define AZIZ_1      i2c_reg[3]
  #define THRESH      i2c_reg[4]
  #define DIM         i2c_reg[5] // PWM values as fractions of 64, or whatever we set the freqency to.
  #define BRIGHT      i2c_reg[6]
  */

#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <i2c/i2c_slave_defs.h>
#include <i2c/i2c_machine.h>
#include "adc.h"

volatile uint8_t i2c_reg[I2C_N_REG];
#define OUTPUT PB1 // OC0B/OC1A

// Function prototypes
void setup_pwm(void);


void setup_pwm()
{
    //TCCR0A = 1<<COM0B1 | 0<<COM0B0 | 1<<WGM01 | 1<<WGM00;
    //TCCR0B = 1<<WGM02;
    TCCR0A = _BV(COM0B1) |  _BV(WGM01) | _BV(WGM00);
    //TCCR0B = _BV(WGM02);
    // disable interrupts
    TIMSK = 0;
    // set TOP to generate ~2 kHz
    OCR0A = 63;// Frequency: 63=2Khz, 127=1Khz, 255=500Hz...
    // Duty cycle. Vary this in main or via I2C as required.
    OCR0B = 31; //31 = 50%
    // start timer at 125 kHz (8 MHz / 64)
    TCCR0B = _BV(WGM02) | _BV(CS01) | _BV(CS00);
}

int main(void)
{
    uint8_t tccr0a;
    uint16_t aziz = 0; // Fifth Element fans will get this.
    DDRB = 0x00;
    DDRB |= _BV(OUTPUT);
    i2c_init();
    sei();
    REG_CTRL = 1; //0 = OFF, 1 = ON auto, 100 = not a real thing
    setup_pwm();
    tccr0a = TCCR0A;
    // Set a 50% PWM initially.
    // These can all be set by I2C/Python on the host machine.
    REG_B = 31;
    THRESH = 100; // 8 bits. So divide ADC threshold by 10
    DIM = 5; // PWM values as fractions of 64
    BRIGHT = 32;


    while(1)
    {
        while(i2c_transaction_ongoing()); // Wait for any i2c activity to stop,
        //i2c_check_stop();
        _delay_us(5);
        if (REG_CTRL == 0) // The PI can instruct the output to be off
        {
            TCCR0A = 0; // Switch off PWM
            continue;
        }
        else if (REG_CTRL == 1)
        {
            TCCR0A = tccr0a; // Restore it and commence PWM
            // Make sure  the pin in low.
            PORTB &= ~_BV(OUTPUT);
            REG_CTRL = 100; // Some unused value as we only want to run this once.
        }
        // Update the registers with ADC values.
        else{ //  AUTO
            aziz = ADC_read(2); // 16 bits
            AZIZ_0 = aziz & 0x00FF;          // Low byte
            AZIZ_1 = ((aziz & 0x0300) >> 8); // High byte
            if(BRIGHT > 64) BRIGHT = 64;     // Behaviour undefined.
            if(aziz > (THRESH * 10))
                OCR0B = DIM; // Dim
            else
                OCR0B = BRIGHT; // Bright
        }
        _delay_ms(50);
    }
}
