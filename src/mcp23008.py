#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Driver for MCP 230008.
'''

import math
import os
import RPi.GPIO as GPIO
#import RPi_I2C_driver
import signal
import sys
import time
import smbus


GPIO.setmode(GPIO.BCM)


def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# Check this with sudo i2cdetect -y 1
IO_ADDRESS = 0x27

# Set this to the board's supply voltage
VOLTAGE =  5.0


class I2CIOHatException(Exception):
    '''
    Just so we can raise an error
    '''
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return "I2CIOHatException, {0}, ".format(self.message)
        else:
            return "Some error in I2CIOHat occurred!"


class I2CIOHat(object):
    '''
    @param a byte with the pins we want to pins as outputs set to 1
    '''
    def __init__(self, addr):
        #try: os.system("sudo rmmod rtc_ds1307")
        #except: pass
        #signal.signal(signal.SIGINT, self.signal_handler)
        # Define the register addresses
        self.bus = smbus.SMBus(1)
        time.sleep(0.5) # Wait for but to settle. https://forums.raspberrypi.com/viewtopic.php?t=203286
        self.clock_addr = 0x68
        self.MCP23008_addr = addr
        self.MCP23008_IODIR = 0x00
        self.MCP23008_IPOL = 0x01
        self.MCP23008_GPINTEN = 0x02
        self.MCP23008_DEFVAL = 0x03
        self.MCP23008_INTCON = 0x04
        self.MCP23008_IOCON = 0x05
        self.MCP23008_GPPU = 0x06
        self.MCP23008_INTCAP = 0x08
        self.MCP23008_INTF = 0x07
        self.MCP23008_GPIO = 0x09
        self.MCP23008_OLAT = 0x0A


    def enable_interrupts(self, pins):
        # Set up interrupts in the MCP23008
        # Enable interrupts
        # IMPORTANT! SET the ODR bit in IOCON to enable open-drain operation of the
        # interrupt pin otherwise the Pi will be over-voltaged on this pin if the board
        # is running on 5V. Add a pullup on the pin to +3V3
        #self.bus.write_byte_data(self.MCP23008_addr,self.MCP23008_IOCON, 0x00)
        #if VOLTAGE == 5.0:
        self.bus.write_byte_data(self.MCP23008_addr,self.MCP23008_IOCON, 0x04)
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_GPINTEN, pins)
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_DEFVAL, pins)
        #self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_INTCON, pins) # Compare to DEFVAL
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_INTCON, 0x00) # Compare to previous

    def enable_pullups(self, pins):
        self.bus.write_byte_data(self.MCP23008_addr,self.MCP23008_GPPU, pins)


    def read_test(self):
        pins = self.bus.read_byte_data(self.MCP23008_addr, self.MCP23008_GPIO)
        pins = self.reverse_mask(0xff - pins)# Reverse and invert
        print(format(pins, "08b"))
        return pins


    def close(self):
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_GPIO, 0b00000000)
        GPIO.remove_event_detect(27)
        '''try:
            os.system("sudo modprobe rtc_ds1307")
        except ex:
            print(ex)'''


    def signal_handler(self,sig, frame):
        print('You pressed Ctrl+C!')
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_GPIO, 0b00000000)
        #GPIO.remove_event_detect(27)
        '''try:
            os.system("sudo modprobe rtc_ds1307")
        except ex:
            print(ex)'''
        sys.exit(0)


    def find_position(self,n):
        # bit 0 is posiotion 1
        if n == 0: return None
        return int(math.log(n & -n, 2)) + 1


    def reverse_mask(self, x):
        x = ((x & 0x55555555) << 1) | ((x & 0xAAAAAAAA) >> 1)
        x = ((x & 0x33333333) << 2) | ((x & 0xCCCCCCCC) >> 2)
        x = ((x & 0x0F0F0F0F) << 4) | ((x & 0xF0F0F0F0) >> 4)
        #x = ((x & 0x00FF00FF) << 8) | ((x & 0xFF00FF00) >> 8)
        #x = ((x & 0x0000FFFF) << 16) | ((x & 0xFFFF0000) >> 16)
        return x


    def read_buttons(self):
        '''
        @param tuple containing the raw pin read and the first button.
        '''
        button = None
        pins = self.bus.read_byte_data(self.MCP23008_addr, self.MCP23008_GPIO)
        pins = self.reverse_mask(0xff - pins) # Reverse and invert
        button = self.find_position(pins)
        return pins, button

    def set_iodir(self, bits):
        '''
        Set the bits data direction.
        1 = input, 0 = output.
        '''
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_IODIR, bits)
        return


    def write(self, data):
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_GPIO, data)
        # write_byte_data returns nothing, so read the register and check if
        # the data match. Raise an exception if not.
        test = self.bus.read_byte_data(self.MCP23008_addr, self.MCP23008_OLAT)
        if test != data:
            raise I2CIOHatException(f"Write to device failed. Data:{data} != {test}.")



    def read_int_cap(self, mask):
        '''
        @param Bit mask to bitwise & to result
        @return tuple containing the raw pin read and the first button.
        '''
        pins = mask & self.bus.read_byte_data(self.MCP23008_addr, self.MCP23008_INTCAP)
        pins = self.reverse_mask( 0xFF - pins) # Invert and reverse
        button = self.find_position(pins)
        return pins, button


# Test malarky
if __name__ == "__main__":
    test = I2CIOHat(IO_ADDRESS, 0xFF)
    test.enable_pullups()
    test.enable_interrupts()
    while True:
        button = test.read_test()
        button = test.find_position(button)
        print(button)
