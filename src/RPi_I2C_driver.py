# -*- coding: utf-8 -*-
"""
Compiled, mashed and generally mutilated 2014-2015 by Denis Pleic
Made available under GNU GENERAL PUBLIC LICENSE

# Modified Python I2C library for Raspberry Pi
# as found on http://www.recantha.co.uk/blog/?p=4849
# Joined existing 'i2c_lib.py' and 'lcddriver.py' into a single library
# added bits and pieces from various sources
# By DenisFromHR (Denis Pleic)
# 2015-02-10, ver 0.1
# Modified by Mike Evans 2024 <mikee@saxicola.co.uk>

"""
#
#
import logging
from smbus import SMBus
from time import *
import time



logger = logging.getLogger(__name__)
logger.setLevel(level=logging.DEBUG)
sh = logging.StreamHandler()
sh_formatter = logging.Formatter('%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
sh.setFormatter(sh_formatter)
logger.addHandler(sh)

DEBUG = logger.debug
INFO = logger.info



class i2c_device:
   def __init__(self, addr, port=1):
      self.addr = addr
      self.bus = SMBus(port)

# Write a single command
   def write_cmd(self, cmd):
      self.bus.write_byte(self.addr, cmd)

# Write a command and argument
   def write_cmd_arg(self, cmd, data):
      self.bus.write_byte_data(self.addr, cmd, data)

# Write a block of data
   def write_block_data(self, cmd, data):
      self.bus.write_block_data(self.addr, cmd, data)

# Read a single byte
   def read(self):
      return self.bus.read_byte(self.addr)

# Read
   def read_data(self, cmd):
      return self.bus.read_byte_data(self.addr, cmd)

# Read a block of data
   def read_block_data(self, cmd):
      return self.bus.read_block_data(self.addr, cmd)



# LCD Address
ADDRESS = 0x3F

# commands
LCD_CLEARDISPLAY = 0x01
LCD_RETURNHOME = 0x02
LCD_ENTRYMODESET = 0x04
LCD_DISPLAYCONTROL = 0x08
LCD_CURSORSHIFT = 0x10
LCD_FUNCTIONSET = 0x20
LCD_SETCGRAMADDR = 0x40
LCD_SETDDRAMADDR = 0x80

# flags for display entry mode
LCD_ENTRYRIGHT = 0x00
LCD_ENTRYLEFT = 0x02
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
LCD_DISPLAYON = 0x04
LCD_DISPLAYOFF = 0x00
LCD_CURSORON = 0x02
LCD_CURSOROFF = 0x00
LCD_BLINKON = 0x01
LCD_BLINKOFF = 0x00

# flags for display/cursor shift
LCD_DISPLAYMOVE = 0x08
LCD_CURSORMOVE = 0x00
LCD_MOVERIGHT = 0x04
LCD_MOVELEFT = 0x00

# flags for function set
LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00
LCD_2LINE = 0x08
LCD_1LINE = 0x00
LCD_5x10DOTS = 0x04
LCD_5x8DOTS = 0x00

# flags for backlight control
LCD_BACKLIGHT = 0x08
LCD_NOBACKLIGHT = 0x00
light = LCD_BACKLIGHT

En = 0b00000100 # Enable bit
Rw = 0b00000010 # Read/Write bit
Rs = 0b00000001 # Register select bit



class lcd():
    """
    Initializes objects and LCD
    @param The I2C address of the display. Should not be fixed.
    """
    def __init__(self, address):
        self.lcd_device = i2c_device(address)
        self.light = LCD_BACKLIGHT
        self.init()
        self.title = ''
        self.oldtitle = ''
        self.scroll_title = ''
        self.looper = 0
        #self.scroll_speed = 1 # Should be set from caller.
        self.busy = False
        self.active = False
        sleep(0.2)


    def LCDdisplay(self, text, line, scroll=True, scroll_speed = 2):
        """
        Display the text on the appropriate line.
        @param The text to display
        @param Int: The line on which to display it, 1 or 2.
        @param Bool: Whether to scroll or not. Default = True. Never used anyway.
        @param The scrolling speed, bigger number = slower scrolling.
        """
        self.scroll_speed = scroll_speed
        if self.busy == True:
            return
        self.busy = True
        scrolling = scroll
        scroll_title = text
        self.title = text
        if len(text) < 17:
            #start = time.time()
            self.lcd_display_string(f'{text}', line)
            self.busy = False
            #DEBUG(time.time() - start)
            return
        if self.title != self.oldtitle:
            self.scroll_title = f"{self.title}  " # Add spaces for wrapping.
        if self.looper % self.scroll_speed == 0:
            self.scroll_title = self.scroll_title[1:] + self.scroll_title[:1]
            self.lcd_display_string(self.scroll_title[:16], line)
        self.oldtitle = self.title
        self.looper += 1
        self.busy = False




    def init(self):
        """
        A separate init method in case we need to re-init the LCD
        """
        self.active = False
        self.lcd_write(0x03)
        self.lcd_write(0x03)
        self.lcd_write(0x03)
        self.lcd_write(0x02)
        self.lcd_write(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE)
        self.lcd_write(LCD_DISPLAYCONTROL | LCD_DISPLAYON)
        self.lcd_write(LCD_CLEARDISPLAY)
        self.lcd_write(LCD_ENTRYMODESET | LCD_ENTRYLEFT)
        self.active = True


    def scroll_left(self): # By one char
        self.lcd_write(LCD_MOVELEFT)
        return


    # clocks EN to latch command
    def lcd_strobe(self, data):
        self.lcd_device.write_cmd(data | En | self.light)
        self.lcd_device.write_cmd(data & ~En | self.light)


    def lcd_write_four_bits(self, data):
        self.lcd_device.write_cmd(data | self.light)
        self.lcd_strobe(data)

    # write a command to lcd
    def lcd_write(self, cmd, mode=0):
        self.lcd_write_four_bits(mode | (cmd & 0xF0))
        self.lcd_write_four_bits(mode | ((cmd << 4) & 0xF0))


    # write a character to lcd (or character rom) 0x09: backlight | RS=DR<
    # works!
    def lcd_write_char(self, charvalue, mode=1):
        self.lcd_write_four_bits(mode | (charvalue & 0xF0))
        self.lcd_write_four_bits(mode | ((charvalue << 4) & 0xF0))



    # put string function
    def lcd_display_string(self, string, line):
        #start = time.time()
        disp_len = 16
        if line == 1:
            self.lcd_write(0x80)
        elif line == 2:
            self.lcd_write(0xC0)
        elif line == 3:
            self.lcd_write(0x94)
        elif line == 4:
            self.lcd_write(0xD4)
        for char in string:
            disp_len -= 1
            self.lcd_write(ord(char), Rs)
        while disp_len > 0: # Pad line with spaces.
            disp_len -= 1
            self.lcd_write(ord(' '), Rs)
        #DEBUG(time.time() - start)

    # clear lcd and set to home
    def lcd_clear(self):
        self.lcd_write(LCD_CLEARDISPLAY)
        self.lcd_write(LCD_RETURNHOME)

    # define backlight on/off (lcd.backlight(1); off= lcd.backlight(0)
    def backlight(self, state): # for state, 1 = on, 0 = off
        if state == 1:
            self.light = LCD_BACKLIGHT
            self.lcd_device.write_cmd(self.light)
        elif state == 0:
            self.light = LCD_NOBACKLIGHT
            self.lcd_device.write_cmd(self.light)

    # add custom characters (0 - 7)
    def lcd_load_custom_chars(self, fontdata):
        self.lcd_write(0x40);
        for char in fontdata:
            for line in char:
                self.lcd_write_char(line)



    # define precise positioning (addition from the forum)
    def lcd_display_string_pos(self, string, line, pos):
        if line == 1:
            pos_new = pos
        elif line == 2:
            pos_new = 0x40 + pos
        elif line == 3:
            pos_new = 0x14 + pos
        elif line == 4:
            pos_new = 0x54 + pos

        self.lcd_write(0x80 + pos_new)

        for char in string:
            self.lcd_write(ord(char), Rs)
