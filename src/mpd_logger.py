#!/bin/env python3
# -*- coding: utf-8 -*-

#  Copyright 2024 Mike Evans <mikee@saxicola.co.uk>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# A stand alone(?) logger for MPD or systems using MPD.
# Usage
# import mpd_logger
# mpd_logger = mpd_logger.mpd_logger('logfile.db')
# Uses python-mpd2
# pip install python-mpd2




from datetime import datetime
from datetime import timedelta
from enum import Enum, IntFlag
import json
import logging
import os
import pprint
import threading
import signal
import time
import subprocess
import sys
import dateutil.parser as dp
#import evdev
#from evdev import InputDevice, categorize # Install with apt-get install python3-dev
# Create dev with: sudo dtoverlay rotary-encoder pin_a=5 pin_b=6 relative_axis=1 steps-per-period=2
# Add to /boot/config.txt dtoverlay=rotary-encoder,pin_a=5,pin_b=6,relative_axis=1,steps-per-period=2
# and perhaps dtoverlay=gpio-key,gpio=26,keycode=28,label="ENTER".
from git import Repo #, Remote
from mpd import MPDClient
import sqlite3
from threading import Thread
#import version
DEVEL = False

here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
this = os.path.join(here, "radio.py")


logger = logging.getLogger(__name__)
logger.setLevel(level=logging.DEBUG)
sh = logging.StreamHandler()
sh_formatter = logging.Formatter('%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
sh.setFormatter(sh_formatter)
logger.addHandler(sh)

DEBUG = logger.debug
INFO = logger.info



class mpd_logger():
    def __init__(self, dbfile='playlog.db'):
        self.create_db(dbfile)
        self.mpdclient = self.mpd_connect()
        #self.mpd_logger = self(os.path.join(here, 'history.db'))
        return

    def mpd_connect(self): # Or reconnect
        mpdclient = None
        try:

            mpdclient  = MPDClient()
            mpdclient.timeout = 5
            mpdclient.idletimeout = 5 #None
            mpdclient.connect("localhost", 6600)
        except:
            raise
            DEBUG("Already connected")
        return mpdclient



    def create_db(self, db_name):
        """
        Create a DB to store radio station data.
        This may make later operations simpler, maybe.
        This could be an in memory DB, maybe.
        """
        self.db = sqlite3.connect(db_name)  # or sqlite3.connect(":memory:")
        self.db.row_factory = sqlite3.Row   #   add this row
        cursor = self.db.cursor()
        cursor.execute('CREATE TABLE IF NOT EXISTS history(start timestamp type UNIQUE, \
            stop timestamp, \
            url text, \
            station text, \
            filename text,\
            title text, note text)')
        self.db.commit()



    def store_play(self, data):
        """
        Insert a play item
        @param data. A dictionary of stuff we want to store
        """
        self.db.row_factory = sqlite3.Row   #   add this row
        cursor = self.db.cursor()
        sql = "INSERT OR IGNORE INTO history (title, filename, start, url, station)                   VALUES(?,?,?,?,?)"
        cursor.execute(sql, (data.get('title'),data.get('filename'),data.get('start'),data.get('url'),data.get('station')))
        self.db.commit()


    def update_history_db(self):
        """
        Add, or not, the current state to the history database.

        """
        data = {}
        data['title'] = self.mpdclient.currentsong().get('title')
        if data['title'] == None:
            data['filename'] = self.mpdclient.currentsong().get('file')
        DEBUG(data)
        # We want the start time of the current track so we subtract elapsed time from now()
        elapsed = self.mpdclient.status().get('elapsed') # elapsed time in seconds.
        DEBUG(elapsed)
        start = datetime.now() - timedelta(seconds=float(self.mpdclient.status().get('elapsed')))
        data['start'] = start.strftime("%Y-%m-%d %H:%M:%S") # We don't want milliseconds so this.
        self.store_play(data)
        return

####################################### END MPD_LOGGER ###################################


def test():
    tester = mpd_logger('test.db')
    tester.update_history_db()
    tester.db.close()


if __name__ == '__main__':
    test()
