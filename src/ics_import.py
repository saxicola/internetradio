#!/usr/bin/env python3


'''
Load an ICS and turn it into this:
[
  {
    "name": "AnonRadio",
    "url": "http://anonradio.net:8000/anonradio",
    "comment": "",
    "genre": "Eclectic"
  },
]

for stations and for alarms:
[
  {
    "name": "Today Program",
    "url": "http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_radio_fourfm/bbc_radio_fourfm.isml/bbc_radio_fourfm-audio%3d96000.norewind.m3u8",
    "t_start": "07:31",
    "duration": "1:30",
    "d_start": null,
    "d_end": null,
    "comment": "",
    "genre": "News",
    "weekday": "Monday"
  },
]
'''

from datetime import datetime
from datetime import timedelta
from icalendar import Calendar, Event
import json
import logging
import pprint




logger = logging.getLogger(__name__)
logger.setLevel(level=logging.DEBUG)
sh = logging.StreamHandler()
sh_formatter = logging.Formatter('%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
sh.setFormatter(sh_formatter)
logger.addHandler(sh)

DEBUG = logger.debug
INFO = logger.info


weekDaysMapping = ("Monday", "Tuesday",
                   "Wednesday", "Thursday",
                   "Friday", "Saturday",
                   "Sunday")

# There's probably python way to do this but...
short_to_long_day = {'MO':"Monday",'TU':"Tuesday",
                   'WE':"Wednesday", 'TH':"Thursday",
                   'FR':"Friday", 'SA':"Saturday",
                   'SU':"Sunday"}
#############################
def get_duration(start, end):
    '''
    @param start datetime object
    @param end datatime object
    @return String represenation of the duration.
    '''
    DEBUG(end)
    DEBUG(start)
    duration = end.replace(tzinfo=None) - start.replace(tzinfo=None) # As a timedelta object
    d_str = ':'.join(str(duration).split(':')[:2])
    return d_str


def read_alarms_ics():
    '''
    Read an alarms and station list from a korganiser ICS file.
    VTODO Contains station list.
    VEVENT Contains alarms list.
    '''
    with open('alarms.ics') as fp:
        calendar = Calendar.from_ical(fp.read())
    alarms = []
    for event in calendar.walk('VEVENT'):
        alarm = {}
        alarm['name'] = event.get("SUMMARY").to_ical().decode()
        alarm['url'] = event.get("LOCATION").to_ical().decode().replace('\\n','').replace(' ','')
        try:
            alarm['genre'] = event.get("CATEGORIES").to_ical().decode()
        except:
            alarm['genre'] = "NONE"
        alarm['dt_start'] = event.get("DTSTART").dt.strftime("%Y-%m-%d %H:%M:%S")
        alarm['dt_end'] = event.get("DTEND").dt.strftime("%Y-%m-%d %H:%M:%S")
        alarm['t_start'] = event.get("DTSTART").dt.strftime("%H:%M")
        alarm['t_end'] = event.get("DTEND").dt.strftime("%H:%M") # Not used directly
        alarm['d_end'] = None #event.get("DTEND").dt.strftime("%Y-%m-%d")
        alarm['d_start'] = None # Neither are used at the moment but are tested.
        d_str = get_duration( event.get("DTSTART").dt, event.get("DTEND").dt)
        alarm['duration'] = d_str
        alarm['weekday'] = weekDaysMapping[event.get("DTSTART").dt.weekday()]
        alarm['daynum'] = event.get("DTSTART").dt.weekday()
        rrule = event.get('RRULE')
        if rrule != None:
            DEBUG(rrule)
            DEBUG(alarm['name'])
            freq = rrule.get('FREQ')[0]
            byday = rrule.get('BYDAY')[0]
            # There are also 'UNTIL' and  'INTERVAL' keys. Which may be useful later.
            try: alarm['until'] = rrule.get('UNTIL')[0].strftime("%Y-%m-%d %H:%M")
            except: alarm['until'] = None
            alarm['interval'] = rrule.get('INTERVAL')
            alarm['weekday'] = short_to_long_day[byday]
            alarm['repeat'] = freq

        alarms.append(alarm)
    stations = []
    for todo in calendar.walk('VTODO'):
        station = {}
        station['name'] = todo.get("SUMMARY").to_ical().decode()
        station['url'] =  todo.get("LOCATION").to_ical().decode().replace('\\n','').replace(' ','')
        station['genre'] = todo.get("CATEGORIES").to_ical().decode()
        station["comment"] = ""
        stations.append(station)
    return stations, alarms



def save_json(data, filename):
    with open(filename, 'w' ) as fp:
        try: json.dump(data, fp, indent = 2)
        except: pprint.pprint(data, fp)# THis won't complain about data types

    return

if __name__ == "__main__":
    stations, alarms = read_alarms_ics()
    #print (stations)
    #print(alarms)
    save_json(alarms, "ical_alarms.json")
    save_json(stations, "ical_stations.json")
