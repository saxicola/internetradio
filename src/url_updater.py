#!/usr/bin/python3

import logging
import json
import urllib.request
import urllib.parse
import os.path


logger = logging.getLogger(__name__)
logger.setLevel(level=logging.DEBUG)
sh = logging.StreamHandler()
sh_formatter = logging.Formatter('%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
sh.setFormatter(sh_formatter)
logger.addHandler(sh)

DEBUG = logger.debug
INFO = logger.info


# TODO. I've only got two(?) URLs here, these need be expanded.
urls = ['http://lstn.lv/bbcradio.m3u8?station=bbc_radio_fourfm&bitrate=48000',
        'http://lstn.lv/bbcradio.m3u8?station=bbc_radio_four_extra&bitrate=48000',
        'http://lstn.lv/bbcradio.m3u8?station=bbc_radio_three&bitrate=96000']
# The files we need to modify.
filenames = ['alarms.json','stations.json']



def update_urls():
    '''
    If we are here then the FBBC have changed the streaming URL's again.
    We can get the new data from:
    http://lstn.lv/bbcradio.m3u8?station=bbc_radio_four_extra&amp;bitrate=48000
    and
    http://lstn.lv/bbcradio.m3u8?station=bbc_radio_fourfm&amp;bitrate=48000
    which seem to be stable and get me a bbcradio.m3u8 file that I can read to get the stream URL.
    '''
    new_urls = []
    for url in urls:
        data = urllib.request.urlopen(url)
        #data = urllib2.urlopen(urls[0]) # it's a file like object and works just like a file
        for line in data: # files are iterable
            if line.decode().startswith('#'):
                continue
            new_urls.append(line.decode())
    for url in new_urls:
        print(url)
    # Now to update the jsons
    for filename in filenames:
        data = None
        with open(filename) as f:
            data = json.load(f)
            for url in new_urls:
                path = urllib.parse.urlparse(url).path
                station = path.split('/')[4]
                for alarm in data: # or station
                    #print(alarm['url'])
                    if station in alarm['url']:
                        DEBUG(f"Fixing {alarm['name']}")
                        alarm['url'] = url
                f.seek(0) # Rewind

        with open(filename, 'w') as f:
            json.dump(data, f, indent=4)
        with open('BBC_URLs.txt', 'w') as f:
            for url in new_urls:
                f.write(f'{url}\n')
    # At this point radio will auto reload the new jsons and we are done! :)
    # It should probably email me.
    # TODO
    import send_a_mail
    send_a_mail.send_mail('Some URLs have been updated.')


def run_tests():
    update_urls()
    return

# Just run it stand alone.
if __name__ == '__main__':
    run_tests()
