#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  radio.py
#
#  Copyright 2022 Mike Evans <mikee@saxicola.co.uk>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
'''
This is an internet radio using a Raspberry Pi and some IO hardware.
In this case the IO card is custom and uses an MCP23008 I2C chip.
Volume and tuning are via a digital encoder with an appropriate dtoverlay entry in /boo/config.txt
to add it to the device tree.
The device is monitored in a thread and the IO card emits an interrupt whenever its input
state changes.
'''

import calendar
from datetime import datetime
from datetime import timedelta
from enum import Enum, IntFlag
import json
import logging
import os
import pprint
import threading
import signal
import time
import subprocess
import sys
import dateutil.parser as dp
import evdev
from evdev import InputDevice, categorize # Install with apt-get install python3-dev
# Create dev with: sudo dtoverlay rotary-encoder pin_a=5 pin_b=6 relative_axis=1 steps-per-period=2
# Add to /boot/config.txt dtoverlay=rotary-encoder,pin_a=5,pin_b=6,relative_axis=1,steps-per-period=2
# and perhaps dtoverlay=gpio-key,gpio=26,keycode=28,label="ENTER".
#from git import Repo #, Remote
import mpd
#from mpd import CommandError
import alsaaudio
from  RPi import GPIO
from mcp23008 import I2CIOHat, I2CIOHatException
import RPi_I2C_driver
import sqlite3
from threading import Thread
import url_updater
import version
import mpd_logger
import lcd_driver

DEVEL = False

here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
this = os.path.join(here, "radio.py")

GPIO.setmode(GPIO.BCM)

# I2C IO card detected at address 0x27 using sudo i2cdetect -y 1

# LCD I2C expansion pinout, looking from botton edge
# 1 GND
# 2 DATA
# 3 CLCK
# 4 +5V

# IO Card encoder pinouts, looking from botton edge
# 1 GND     PIN C
# 2 GPIO5   PIN A   CLK
# 3 GPIO6   PIN B   DT
# 4 GPIO26  SWITCH D or E

# Encoder pins
e_clk = 5 # pin 29
e_dt = 6 # pin 31
e_sw = 26 # pin 37

io_int = 23     # MCP23008 interrupt line on IO card. Hacked a bit as 27 is dead.

powermon = 22 # Powermon board uses this. Can we share it?


# Programming will be via the web interface.
example_media = {'idx': 1,'name':'BBC Radio 4', 'url':'http://stream.live.vc.bbcmedia.co.uk/bbc_radio_fourfm', 'd_start':None,'t_start':None, 'd_end':None,'t_end': None, 'comment':None , 'genre':'talk radio'}


logger = logging.getLogger(__name__)
logger.setLevel(level=logging.DEBUG)
sh = logging.StreamHandler()
sh_formatter = logging.Formatter('%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
sh.setFormatter(sh_formatter)
logger.addHandler(sh)

DEBUG = logger.debug
INFO = logger.info

# TODO: Needs a way to set these on start. Possibly with a config
# That the user can easily change. Web debugging perhaps?
#import debug
#if debug.mode = None:
#etc...
#logging.disable(logging.DEBUG)
#logging.disable(logging.INFO)


# Describe the numitron segments
segs7 = [
0b01111110, # 0
0b00000110, # 1 or self.hh.write(0b0000110) to shift the 1 to the left
0b01101101, # 2
0b01111001, # 3
0b00110011, # 4
0b01011011, # 5
0b01011111, # 6
0b01110000, # 7
0b01111111, # 8
0b01111011, # 9
0b01110111, # A
0b00011111, # b
0b01001110, # C
0b00111101, # d
0b01001111, # E
0b01000111, # F
]


class Modes(IntFlag):
    """
    Modes loosely reflect switch positions. Up = 1 for both switches
    Except for > 4 which is for when the knob is pushed to get tuning mode.
    """
    STANDBY     = 0 # All down
    ALARM       = 1 # RH up alarm only
    PLAY        = 2 # LH up
    PLAY_PLUS   = 3 # Both up
    TUNING_0    = 4 # Genre select
    TUNING_1    = 5 # Station select
    TUNING_2    = 6 # Save and setup



######################################################################################
# This really doesn't need to be a class and I should rewrite ALL the code.
# However, there was a point originally and now it's too late.
class Radio():
    '''
    Docstring
    '''
    MAX_VOL = 65
    looper = 0
    def __init__(self):
        self.eq_list = [54,54,66,66,66,66,66,66,82,86]
        self.station = 0
        self.alarm_list = None
        self.play_file = os.path.join(here, 'stations.json')
        self.alarm_file = os.path.join(here, 'alarms.json')
        self.file_time = os.stat(self.play_file).st_mtime # Remember the file timestamp
        self.alarm_time = os.stat(self.alarm_file).st_mtime # Remember the file timestamp
        self.this_time = os.stat(this).st_mtime # Use this for restart on update
        self.mixer = alsaaudio.Mixer('Headphone', cardindex=0, device='compressor')
        #self.mixer = alsaaudio.Mixer('Headphone', cardindex=0, device='equal')
        #self.set_eq(self.eq_list)
        self.playlist = [] # List of dicts from json file
        self.mpdclient = None
        self.mpd_connect()
        self.playlist = self.read_playlist_file(self.play_file) # From disk
        self.load_playlist() # Into mpd
        self.read_state_file()
        self.t_state = 'OFF'
        self.init_numitrons()
        GPIO.setup(e_sw,    GPIO.IN, pull_up_down=GPIO.PUD_UP)
        # Powermon pin.
        self.switch_board = I2CIOHat(0x27)
        self.switch_board.set_iodir(0xF0) #  Low nibble as output, high nibble as input. Not PCB is backwards.
        self.switch_board.enable_interrupts(0xF0)
        self.switch_board.enable_pullups(0xF0)
        # Trap CTRL+C
        signal.signal(signal.SIGINT, self.sig_handler)
        # Knob press
        GPIO.add_event_detect(e_sw, GPIO.FALLING, callback=self.knob_pressed_callback, bouncetime=100)
        # switch board setup
        GPIO.setup(io_int,  GPIO.IN) # Has a pullup resistor
        GPIO.add_event_detect(io_int, GPIO.FALLING, callback=self.switch_callback) #, bouncetime=10)
        # Switch toggle.
        self.knobby = 0 # +1, -1 or 0
        self.modes = Modes
        self.mode = self.modes.STANDBY # Standby This will be changed in the switch read.
        self.old_mode = self.mode
        self.volume = 60 # /100
        self.mixer.setvolume(self.volume)
        self.mute = False # Muted?
        self.process_timeout = None
        # Power up the LCD. Turn on the transistor at GP1
        self.switch_board.write(0x00) # Power off the LCD
        time.sleep(0.2) # Make sure it's off to ensure a reset
        self.switch_board.write(0x02) # Back on again
        time.sleep(0.1)
        self.lcd = RPi_I2C_driver.lcd(0x3F)
        self.lcd.lcd_clear()
        self.lcd.backlight(1)
        self.lcd_active = True
        self.alarm_list = []
        self.alrm_idx = None
        self.alarmed = None
        self.current_playing = None
        self.mpd_logger = mpd_logger.mpd_logger(os.path.join(here, 'history.db'))
        # Run the device tree reader for the encode in a thread.
        self.re = threading.Thread(target=self.read_encoder)
        self.re.daemon = True
        self.re.start()
        self.led = 0
        # Numitrons can take car of themselves.
        self.bl = threading.Thread(target=self.numitron)
        self.bl.daemon = True
        self.bl.start()
        self.switch_callback() # Do an initial read of the switches.



    def update_history_db(self):
        """
        Add, or not, the current state to the history database.

        """
        data = {}
        data['title'] = self.mpdclient.currentsong().get('title')
        if data['title'] == None:
            data['filename'] = self.mpdclient.currentsong().get('file')
        #DEBUG(data)
        # We want the start time of the current track so we subtract elapsed time from now()
        elapsed = self.mpdclient.status().get('elapsed') # elapsed time in seconds.
        #DEBUG(elapsed)
        start = datetime.now() - timedelta(seconds=float(self.mpdclient.status().get('elapsed')))
        data['start'] = start.strftime("%Y-%m-%d %H:%M:%S") # We don't want milliseconds so this.
        data['title'] = self.mpdclient.currentsong().get('title')
        self.mpd_logger.store_play(data)
        return


    ######################################################################################
    def dim_numitrons(self):
        """
        Control the dimming of the numitrons via an tiny85 as the I2C device.
        This may never be implemented. Requires a board to be made.
        I've noticed that to set a register value sometimes requires a write, followed
        by a read from the same register.No idea why but I've also seen it mentioned in forums.
        The registers.
        #define REG_CTRL    i2c_reg[0] // For controlling ON/OFF of the numitrons.
        #define REG_B       i2c_reg[1] // OCR0B value to control PWM width.
        #define AZIZ_0      i2c_reg[2] // Light Aziz, light! Lower byte
        #define AZIZ_1      i2c_reg[3] // Upper byte.
        #define THRESH      i2c_reg[4] // ADC value / 10
        #define DIM         i2c_reg[5] // PWM values as fractions of 64 for dim numitrons
        #define BRIGHT      i2c_reg[6] // PWM values as fractions of 64 for bright numitrons
        Most things can be set via writing to the registers.
        """
        # Examples:
        # Set the threshhold
        bus = smbus.SMBus(1)
        IO_ADDRESS = 0x40
        bus.write_byte_data(IO_ADDRESS,4, 60) # 60 is x'd 10 in the tiny to give 600
        bus.read_byte_data(IO_ADDRESS,4)
        # Read and print the light value from the registers, AXIZ_0, AZIZ_1
        print(bus.read_byte_data(IO_ADDRESS,2) + (bus.read_byte_data(IO_ADDRESS,3)*256))
        # Turn off PWM and thus the NUMITRONS
        bus.write_byte_data(IO_ADDRESS,0,0)
        bus.read_byte_data(IO_ADDRESS,0)


    ######################################################################################
    def set_eq(self, eq_list):
        '''
        Set the EQ.
        Can't find a way to do this python-like, need to make system calls.
        Should be called by long knob action, probably.
        Should be a loop too. :)
        @param List of 10 EQ settings to use.
        '''
        os.system("sudo -H -u mpd amixer -D equal -q set '00. 31 Hz' " +  str(eq_list[0]))
        os.system("sudo -H -u mpd amixer -D equal -q set '01. 63 Hz' " +  str(eq_list[1]))
        os.system("sudo -H -u mpd amixer -D equal -q set '02. 125 Hz' " +  str(eq_list[2]))
        os.system("sudo -H -u mpd amixer -D equal -q set '03. 250 Hz' " +  str(eq_list[3]))
        os.system("sudo -H -u mpd amixer -D equal -q set '04. 500 Hz' " +  str(eq_list[4]))
        os.system("sudo -H -u mpd amixer -D equal -q set '05. 1 kHz' " +  str(eq_list[5]))
        os.system("sudo -H -u mpd amixer -D equal -q set '06. 2 kHz' " +  str(eq_list[6]))
        os.system("sudo -H -u mpd amixer -D equal -q set '07. 4 kHz' " +  str(eq_list[7]))
        os.system("sudo -H -u mpd amixer -D equal -q set '08. 8 kHz' " +  str(eq_list[8]))
        os.system("sudo -H -u mpd amixer -D equal -q set '09. 16 kHz' " +  str(eq_list[9]))


    ######################################################################################
    def set_preset_eq(self):
        '''
        Bug-321 TODO
        User can scroll through a list preset EQ's
        Pass the selected EQ to set_eq(self, eq_list)
        '''
        eq_list = [
        {'name':'treble_boost', 'eqs':[54,54,66,66,66,66,66,66,82,86]},
        {'name':'bass_boost', 'eqs':[86,85,66,66,66,66,66,66,80,80]},
        ]


    ######################################################################################
    def parse_time(self, timestr):
        '''
        Format input to XX:XX time format
        Times can therefore be input as '1050' and makes input easier.
        @param String: The time as a string
        '''
        try:
            timestr = datetime.strftime(datetime.strptime(timestr, "%H%M"), "%H:%M")
        except:
            timestr = datetime.strftime(datetime.strptime(timestr, "%H:%M"), "%H:%M")
        return timestr


    ######################################################################################
    def init_numitrons(self):
        '''
        The Numitron board is self contained and runs off the 12V supply directly.
        It's not an actual I2CIOHat though, we are just re-using the same code as the chips
        are the same. MCP23008.
        A loss in power means the drive chips need resetting on power restore.
        '''
        try:
            # Numitron drivers. If present.
            self.hh = I2CIOHat(0x23)
            self.h = I2CIOHat(0x22)
            self.mm = I2CIOHat(0x21)
            self.m = I2CIOHat(0x20)
            self.hh.set_iodir(0x00)
            self.h.set_iodir(0x00)
            self.mm.set_iodir(0x00)
            self.m.set_iodir(0x00)
            self.hh.enable_pullups(0xFF)
            self.h.enable_pullups(0xFF)
            self.mm.enable_pullups(0xFF)
            self.m.enable_pullups(0xFF)
            #INFO(f"Numitrons are GO!. {type(self.hh)}")
        except Exception as ex:
            INFO(str(ex))
            INFO("Could not init numitrons!")


    ######################################################################################
    def numitron(self):
        '''
        This runs in a thread. Nothing calls this.
        Write to the numitrons.
        '''
        while(1):
            self.led ^= 1
            H = time.strftime('%H') # String
            M = time.strftime('%M') # String
            h = int(H[1])
            hh = int(H[0])
            m = int(M[1])
            mm = int(M[0])
            # Write to the numitron drivers
            try:
                # Bug-365 Turn off tubes during the day to prolong their life.
                if (self.mode == self.modes.ALARM or self.mode == self.modes.STANDBY) and (time.localtime().tm_hour >= 9 and time.localtime().tm_hour < 22):
                    self.hh.write(0)
                    self.h.write(0)
                    self.mm.write(0)
                    self.m.write(0)
                    time.sleep(1)
                    continue
                if hh == 1: # Allow for my faulty tube.
                    self.hh.write(0b0000110)
                elif hh == 2:
                    self.hh.write(segs7[hh])
                else:
                    self.hh.write(0b00000000)
                    #self.hh.write(0b01100011)
                self.h.write(segs7[h])
                self.mm.write(segs7[mm])
                self.m.write(segs7[m] ^(self.led << 7)) # This driver also has a seconds bit
            except Exception as e: # This can fail if power has been cycled. See Bug-360
                INFO(str(e))
                INFO("Re-init'ing Numitrons.")
                self.init_numitrons()
            time.sleep(1)


    ######################################################################################
    def check_and_update(self):
        '''
        Phone home and auto update software if needed.
        Unlikely I'll ever use this.
        Only run on power-up, which should be a rare event.
        Using git would be a likely method.
        '''
        self.main().display("CHECKING SOFTWARE", 1)
        self.main().display("VERSION          ", 2)
        repo = Repo("https://gitlab.com/saxicola/internetradio.git")
        o = repo.remotes.origin
        # Should probably check status first.
        self.main().display("UPDATING SOFTWARE", 1)
        o.pull()
        self.main().display("REBOOTING        ", 1)
        self.main().display("PLEASE WAIT      ", 1)


    ######################################################################################
    def mpd_connect(self): # Or reconnect
        try:
            if not self.mpdclient:
                self.mpdclient  = mpd.MPDClient()
            else: # if a client exists, disconnect
                try:
                    self.mpdclient.close()
                # If that fails, don't worry, just ignore it and disconnect
                except(mpd.MPDError, IOError):
                    pass
                try:
                    self.mpdclient.disconnect()
                    # Disconnecting failed, so use a new client object instead
                    # This should never happen.  If it does, something is seriously broken,
                    # and the client object shouldn't be trusted to be re-used.
                except(mpd.MPDError, IOError):
                    self.mpdclient = mpd.MPDClient()
                except mpd.base.ConnectionError:
                    self.mpdclient = mpd.MPDClient()
                return
            #self.mpdclient.idletimeout = 10
            self.mpdclient.connect("localhost", 6600)
            #self.mpdclient.timeout = 10
            self.mpdclient.replay_gain_mode('auto')
            time.sleep(2)
        except IOError as err:
            DEBUG(err)
            errno, strerror = err
            DEBUG(strerror)
        except mpd.MPDError as e:
            DEBUG(e)


    ######################################################################################
    def load_playlist(self):
        # Get the playlist file
        # And add it to the playlist.
        #if self.db:
        #    cursor = self.db.cursor()
        try:  # Can raise ConnectionError("Not connected") See Bug-367
            self.mpdclient.clear()
        except ConnectionError as ce:
            # This fails if mpd is dead.and will crash the radio until systemd restarts mpd_connect
            # Ideally we should call a systemctl restart mpd
            try:
                self.mpd_connect()
            except ConnectionError as ce:
                INFO(str(ce))
                os.system("sudo systemctl restart mpd")

        self.playlist.sort(key = lambda x: (x is None, x['name']))
        for item in self.playlist:
            self.mpdclient.add(item['url'])
            #if self.db:
            #    cursor.execute("INSERT INTO stations()")


    ######################################################################################
    def read_playlist_file(self, filename):
        '''
        Runs on startup
        Read the playlist from the file and add an index in read order.
        Should items have a persistent index number? Dunno.
        Whadda we do if this fails and crashes the radio?
        The status file is a json.dump(self.mpdclient.status()
        which has limited usefulness, especially if the playlist get re-sorted when a
        new item is added.
        @param String: The file name.
        '''
        time.sleep(0.5) # Stop it reading too soon after the file is written
        playlist = []
        try:
            with open(os.path.join(here,filename), 'r') as fp:
                playlist = json.loads(fp.read())
        except Exception as ex:
            INFO(str(ex))
            self.restart()
        # Add an index value
        for idx, item in enumerate(self.playlist):
            item['idx'] = idx
        return playlist


    ######################################################################################
    def read_state_file(self):
        """
        Read the saved state file, or the backup.
        Call this AFTER loading the playlist.
        This will contain, at least, the last playing station and the volume.
        """
        if len(self.playlist) == 0: # Make sure there's a playlist.
            return
        try:
            with open(os.path.join(here,"state.json"), 'r') as stf:
                state = json.loads(stf.read())
            INFO(state)
            self.station = int(state['song'])
        except Exception as ex:
            INFO(str(ex))
            INFO("Cannot open state file. Opening backup and recreating.")
            # Open the backup.
            with open(os.path.join(here,"state-test.json"), 'r') as stf:
                state = json.loads(stf.read())
            # Write out a state.json
            with open(os.path.join(here,"state.json"), 'w') as stf:
                json.dump(state, stf, indent=2)


    ######################################################################################
    def restart(self):
        '''
        This should probably save some state data, to be reloaded after reboot.
        '''
        state = {}
        state['volume'] = self.mixer.getvolume()[0]
        self.mpdclient.stop()
        try:
            station = self.playlist[self.station]
            state['station'] = station
        except Exception as ex:
            INFO(str(ex))
            INFO(state)
        with open(os.path.join(here,"state.json"),'w') as stf:
            json.dump(self.mpdclient.status(), stf, indent=2)

        with open(os.path.join(here,"state-test.json"),'w') as stf:
            json.dump(state, stf, indent=2)

        if  DEVEL == True: # Because we are running from the command line
            print("Restart me")
        else:
            subprocess.check_output("systemctl restart radio", shell=True)


    ######################################################################################
    def save_playlist(self, filename):
        '''
        Save playlist to a file as a json thingy
        @param String: The file name
        '''
        with open(os.path.join(here,self.play_file), 'r') as fp:
            json.dump(self.playlist, fp, indent=2)



    ######################################################################################
    def switch_callback(self, int_pin=None):
        '''
        Switch call back runs when interrupt fires it.
        '''
        DEBUG(int_pin)
        switch = self.switch_board.read_buttons()
        INFO(switch)
        INFO(switch[0] & 0x0F)
        if self.mode < self.modes.TUNING_0: # Set by knob press
            self.mode = self.modes(switch[0] & 0x0F)
        if self.old_mode != self.mode: # A switch was flipped
            #if self.old_mode == self.modes.PLAY_PLUS and self.mode == self.modes.ALARM: # Force the rebuild of the alarms list
            #    self.alarm_list = None
            if self.mode == self.modes.ALARM:
                DEBUG(f"Changing to mode {self.mode}")
                try:
                    self.mpdclient.stop()
                except Exception as e:
                    DEBUG(str(e))
                    pass
                self.alarm_list = None
            elif self.mode == self.modes.STANDBY:
                DEBUG(f"Changing to mode {self.mode}")
                self.mpdclient.stop()
                self.alarm_list = None
            #    self.init_numitrons() # Because sometimes they need restarting.
            elif self.mode == self.modes.PLAY: # Normal play mode
                DEBUG(f"Changing to mode {self.mode}")
                self.alarmed = None
                self.alarm_list = None
            while self.lcd.busy == True: # Need to wait for other writes to complete.
                pass
            self.display("                " , 2) # Remove left over stuff.
        INFO(self.mode)
        #INFO(f'switch: 0x{switch[0]:X}, self.mode: {self.mode}, self.old_mode: {self.old_mode}')
        self.old_mode = self.mode
        #time.sleep(0.2)



    ######################################################################################
    def knob_pressed_callback(self, channel):
        '''
        ISR for encoder switch.
        DONE: The mode needs a timeout to reset back to run mode 0.
        TODO: Bug-361 Discriminate between long and short press and do correct action,
        need to read e_sw in a loop and count
        Meanwhile just toggle in/out of tuning mode.
        @param The originating pin on the 40 pin header.
        '''
        if self.mode == self.modes.STANDBY: return # Standby mode, nothing to do.
        if self.mode < self.modes.TUNING_0:
            self.mode = self.modes.TUNING_0 # Genre select
        elif self.mode == self.modes.TUNING_0:
            self.mode = self.modes.TUNING_1 # Station select
        elif self.mode == self.modes.TUNING_1:
            self.mode = self.modes.TUNING_2 # Station selected
        INFO(self.mode)



    ######################################################################################
    def read_encoder(self):
        '''
        # Run this in a thread to read the encoder as a mouse-like device
        self.knobby is only ever set to 1 or -1.
        '''
        d = evdev.InputDevice('/dev/input/by-path/platform-rotary@6-event')
        for e in d.read_loop():
            if e.type == evdev.ecodes.EV_REL:
                self.knobby = e.value # I should reverse the AB pin
                if self.mode <= self.modes.PLAY_PLUS: #
                    self.do_volume()



    ######################################################################################
    def sig_handler(self, sig, frame):
        INFO('Quitting on Ctrl+C')
        #self.switch_board.close()
        with open(os.path.join(here,"state.json"),'w') as stf:
            json.dump(self.mpdclient.status(), stf, indent=2) # Just a test TODO.
        try:
            self.mpdclient.stop()
            self.mpdclient.close()
            self.mpdclient.disconnect()
        except Exception as ex:
            INFO(str(ex))
            self.mpd_connect()
            self.mpdclient.stop()
            self.mpdclient.close()
            self.mpdclient.disconnect()
        GPIO.cleanup()
        try:
            os.system("sudo modprobe rtc_ds1307")
        except Exception as ex:
            INFO(str(ex))
            pass # Or what?
        sys.exit(0)


    ######################################################################################
    def sort_playlist():
        self.playlist.sort(key = lambda x : x.start, reverse=False)



    ######################################################################################
    def format_time(self, text):
        ''' Format possibly malformed input to XX:XX time format
        @param String time
        @return String time properly formatted.
        '''
        try:
            text = datetime.strftime(datetime.strptime(text, "%H%M"), "%H:%M")
        except:
            text = datetime.strftime(datetime.strptime(text, "%H:%M"), "%H:%M")
        return text


    ######################################################################################
    def do_volume(self):
        self.volume = self.mixer.getvolume()[0]
        #INFO(self.volume)
        if self.knobby != 0:
            self.volume += (self.knobby * 1)
            self.knobby = 0
            if self.volume < 0:
                self.volume = 0
            if self.volume > self.MAX_VOL:
                self.volume =  self.MAX_VOL
            self.mixer.setvolume(self.volume)


    ######################################################################################
    def fix_time(self, timestr):
        '''
        Sometimes times are typed without colons we need to fix this.
        The web interface now deals with this anyway.
        Note: This only fixes missing colons at present.
        @param String time as a (possibly malformed) string
        @return String of the fixed time
        '''
        try:
            time = datetime.strftime(datetime.strptime(timestr, "%H:%M"), "%H:%M")
        except ValueError:
            INFO(f"Need to fix a mis-typed time. {timestr}")
            time = datetime.strftime(datetime.strptime(timestr, "%H%M"), "%H:%M")
        return time


    #############################
    def fix_midnight_spanning(self, item):
        # Bug-345
        # If the alarm list is CREATED when an alarm item spans NOW and midnight
        # then the date may need to be adjusted.
        now =  datetime.now()
        if item['start'].time() > item['stop'].time(): # Midnight spanning
            if now.time() < item['stop'].time():
                if now < item['start']: # Then now is between start and stop.
                    INFO("Adjusted the date.")
                    # Subtract a day from start and stop
                    item['start'] = item['start'] - timedelta(days=1)
                    item['stop'] = item['stop'] - timedelta(days=1)
        return item


    ######################################################################################
    def mark_invalid(self, item):
        '''
        Mark items as invalid if the following conditions are met:
        @param The play item to test.
        @return A playlist object
        '''
        item['valid'] = 1 # Set to 0 in the tests below.
        #INFO(item['name'])
        # Remove items with no start time
        if item['t_start'] == '' or item['t_start'] is None: # Not an alarm
            item['valid'] = 0
            INFO(f"Removing {item['name']} from alarms.")
        # Remove items with no duration
        if item['duration'] == '' or item['duration'] is None: #Invalid item
            item['valid'] = 0
            INFO(f"Removing {item['name']} from alarms.")
        if item['d_end']:
            # This may be a weekly/daily play that spans multiple days/weeks.
            end_date = datetime.strptime(item['d_end'],"%Y-%m-%d")
            # Remove expired items
            if end_date.date() < datetime.now().date():
                # Its time to shine is over
                item['valid'] = 0
                INFO(f"Removing {item['name']} from alarms. It has expired.")
        return item


    #############################
    def fix_playdates(self, item):
        """
        @param A playlist item.
        @return A playlist item.
        """
        if item['weekday'] != '': # We have a weekday. Calc the next play time.
            today = datetime.now().weekday()
            daynum = [i for i, x in enumerate(list(calendar.day_abbr)) if x == item['weekday'][0:3].title()][0]
            item['daynum'] = daynum
            if today > daynum: # If the next play is in the future
                #DEBUG("Advancing the week.")
                item['start'] = item['start'] + timedelta(days=(7 - (today - daynum)))
                item['stop'] = item['stop'] + timedelta(days=(7 - (today - daynum)))
            if today < daynum: # If the next play is in the future
                #DEBUG("Advancing the days.")
                item['start'] = item['start'] + timedelta(days=((daynum - today)))
                item['stop'] = item['stop'] + timedelta(days=((daynum - today)))
        return item

##################################################################################
    def set_alarm(self):
        '''
        Set up the correct alarm into the playlist
        Run this from mode_alarm() each time it's  called
        Arguably I should rename this.
        TODO: Bug-374: How to prioritise alarms? If one is a daily and another has a
        day and they overlap which one to play?
        '''
        if self.alarm_list == None:
            self.make_alarms()
            return
        for item in self.alarm_list:
            #INFO(item)
            duration = dp.parse(item.get('duration'))
            end_time = (item.get('start') + timedelta(minutes=duration.minute, hours=duration.hour))
            if datetime.now() >= item.get('start') and datetime.now() <= end_time:
                #DEBUG(item)
                try:
                    self.current_playing = self.playlist[self.station]
                except Exception as ex:
                    INFO(ex)
                    INFO("Cannot save last playing.") # Because it may not exist yet.
                # Only play if it not playing already or is the set program
                try:
                    self.volume = 60 # /100 Initial volume
                    self.mixer.setvolume(self.volume)
                    # Add it to the end of the playlist
                    self.mpdclient.add(item.get('url'))
                    self.alrm_idx = len(self.mpdclient.playlistinfo())-1
                    self.mpdclient.play(self.alrm_idx)
                    INFO( f"Playing {item.get('name')}" )
                    # Get the list index of the playing item
                    self.alarmed = self.alarm_list.index(item)
                    INFO(item)
                    INFO(f"{item.get('name')} stops at {item.get('stop')}")
                except Exception as ex: # Probably nothing playing so status['song'] may not exist
                    INFO(str(ex))
                    INFO("No song yet.")


#######################################################################################
    def make_alarms(self):
        '''
        Create a valid alarm list sorted by start datetime
        @return List of valid alarms.
        TODO: Bug-374 Alarms with specific dates/days should probably override those without.
        '''
        INFO("Making alarm list")
        #DEBUG("Reloading the alarm list.")
        self.alarm_list = []
        alarm_list = self.read_playlist_file("alarms.json")
        for item in alarm_list:
            #INFO(item)
            daystr = item.get('weekday')
            if daystr != None and daystr != "":
                item['daynum'] = [i for i, x in enumerate(list(calendar.day_abbr)) if x == daystr[0:3].title()][0]
            else:
                item['daynum'] =  datetime.now().weekday() # Used for sorting later. May be modded in fix_playdates()
            self.mark_invalid(item)
            # Having removed invalid items...
            item['start'] = dp.parse(self.fix_time(item['t_start'])) # This assumes the day to be today but...
            duration = dp.parse(self.fix_time(item['duration']))
            item['stop'] = item['start'] + timedelta(minutes=duration.minute, hours=duration.hour)
            # Allow for null in the .jason file
            if item['weekday'] == None: # Change to empty string
                item['weekday'] = ''
            item = self.fix_playdates(item)
            item = self.fix_midnight_spanning(item)

            if item['valid'] == 1:
                self.alarm_list.append(item) # Yay! We have a winner.
        # First sort by datetime start...
        try:
            self.alarm_list.sort(key = lambda x: x['start']) #datetime.strptime(x['start'],'%H:%M'))
        except:
            self.make_alarms()
        # then sort so any items with a weekday are prioritised.
        # One could argue that sorting is not needed but...
        self.alarm_list.sort(key = lambda x: (x is None, x['daynum']))
        #INFO(pprint.pformat(self.alarm_list))
        with open(os.path.join(here,'alarm_list'), 'w') as fout:
            pprint.pprint(self.alarm_list, fout)
        return self.alarm_list


    ######################################################################################
    def display(self, text, line, scroll=True):
        """
        TODO Use this one.
        TODO Use the LCD built-in commands to scroll the display.
        @param The text to display
        @param Int: The line on which to display it, 1 or 2.
        @param Bool: Whether to scroll or not. Default = True
        """
        if self.lcd_active == False: return
        while self.lcd.busy == True:
            pass
        # LCDdisplay(self, text, line, scroll=True, scroll_speed=2):
        #start = time.time()
        self.lcd.LCDdisplay(text, line, scroll, scroll_speed = 3)
        #DEBUG(time.time() - start)


    ######################################################################################
    def main(self):
        ''' Closure function for containing everything called in the loop, below
        '''
        #tuning_mode = 0 # To enable sub-menus we need menu levels.
        self.alarm_list = None
        self.lcd.backlight(1) #  ON until unformed otherwise.
        ######################################################################################
        display = self.display
        """
        So we can just call display()
        """


    ####################################################################
        def mode_play_plus():
            """
            If an  alarm in playing, return to alarm mode, othewise set mode to PLAY
            """
            if self.alarmed != None: # An alarm is playing.
                DEBUG("Setting mode to ALARM")
                self.mode = self.modes.ALARM
            else:
                DEBUG("Setting mode to PLAY")
                self.mode = self.modes.PLAY


        ################################################################
        def mode_play():
            start = time.time()
            # Normal run mode.
            if self.alarm_list:
                self.alarm_list == None
            try:
                status = self.mpdclient.status()
            except Exception as ex:
                INFO(str(ex))
                self.mpd_connect()
                #status = self.mpdclient.status()
                #DEBUG(status)
            if status.get('state') == 'stop':
                try:
                    self.mpdclient.play(int(self.station))
                except Exception as ex:
                    INFO(f"MPD player failed.")
                    INFO(str(ex))
                    self.mpd_connect()
                    time.sleep(1)
            #INFO(self.playlist[int(self.station)])
            try:
                name = self.playlist[int(self.station)].get('name') # Station Name
            except Exception as ex: # Nothing else we can do. Probably not tuned to a station yet?
                # It's also likely the the FBBC have changed their URL's again.
                DEBUG(str(ex))
                self.station = 0 # set a default to avoid the same error again.
                #url_updater.update_urls() # This not working.
                return
            if name:
                display(name, 1)
            else:
                display(""*16, 1)
            try: # song['name']} may not exist yet.
                title = self.mpdclient.currentsong().get('title')
                if title is not None:
                    display(f"{title}", 2)
                else:
                    display(" NO TITLE FOUND!", 2)
            except Exception as ex:
                INFO(str(ex))
                display("                ", 2) #
                DEBUG("Display station name failed")
            #self.update_history_db()


        ######################################################################################
        def mode_tuning():
            '''
            Tune a radio station using the knob
            Tuning will have a directory-like structure, first selecting genre then station.
            '''
            # Define some local variables.
            knob_sensitivity = 2 # More is less :)
            mode_tuning.genres = getattr(mode_tuning,'genres',set())
            mode_tuning.stations = getattr(mode_tuning, 'stations', [])
            mode_tuning.genre = getattr(mode_tuning,'genre','')
            mode_tuning.knobby = getattr(mode_tuning,'knobby', 0)
            mode_tuning.timer = getattr(mode_tuning,'timer', time.perf_counter())
            mode_tuning.last_mode = getattr(mode_tuning,'last_mode', 0)
            timeout = 30 # Seconds to time out this function
            #self.old_mode = self.mode
            def cleanup():
                del mode_tuning.genres
                del mode_tuning.stations
                del mode_tuning.genre
                del mode_tuning.knobby
                del mode_tuning.timer
                del mode_tuning.last_mode
                self.mode = self.old_mode
            if time.perf_counter() > mode_tuning.timer + timeout:
                DEBUG("KNOB TIMEOUT")
                mode_tuning.knobby = 0
                cleanup()
                return

            # Create a set of genres
            if len(mode_tuning.genres) == 0:
                for st in self.playlist:
                    if st['genre'] == '':
                        st['genre'] = 'none'
                    mode_tuning.genres.add(st['genre']) # Using a set to avoid duplicates
            mode_tuning.genres = list(mode_tuning.genres) # Which we now convert to a list.
            mode_tuning.genres.sort()
            self.alarmed = None
            self.alarm_list = None
            #self.mpd_connect()
            #status = self.mpdclient.status()
            self.lcd.backlight(1)

            # Get knobby and set stuff
            # BUG-461 Too sensitive.
            #if self.knobby >= knob_sensitivity:
            mode_tuning.knobby += self.knobby
            self.knobby = 0
            if mode_tuning.knobby < 0:
                mode_tuning.knobby = 0
            selection = int(mode_tuning.knobby / knob_sensitivity)

            # Genre selection mode tuning stage 0
            if self.mode == self.modes.TUNING_0:
                mode_tuning.last_mode = self.mode
                if selection >= len(mode_tuning.genres) - 1:
                    selection = len(mode_tuning.genres) - 1
                display("    SET GENRE   ", 1)
                mode_tuning.genre = mode_tuning.genres[selection]
                display(mode_tuning.genre, 2)

            # Station select mode
            elif self.mode == self.modes.TUNING_1:
                # We need to re-zero the selection to avoid stuff.
                if mode_tuning.last_mode == self.modes.TUNING_0:
                    mode_tuning.knobby = 0
                    mode_tuning.last_mode = self.mode
                if selection >= len(mode_tuning.stations) - 1:
                    selection = len(mode_tuning.stations) - 1
                if len(mode_tuning.stations) == 0:
                    for i, st in enumerate(self.playlist):
                        st['id'] = i # Add an id we can refer to later
                        if mode_tuning.genre == st['genre']:
                            mode_tuning.stations.append(st)
                display("  SET STATION  ", 1)
                mode_tuning.station =  mode_tuning.stations[selection]
                display(mode_tuning.station['name'], 2)

            # Tuning complete. Yay!
            elif self.mode == self.modes.TUNING_2:
                playlist = self.mpdclient.playlistinfo()
                p = self.mpdclient.playlistsearch('file', mode_tuning.station['url'])
                self.mpdclient.stop()
                self.station = mode_tuning.station['id'] # Needs to be an int
                mode_tuning.knobby = 0
                cleanup()
            #INFO(self.mode)


        ######################################################################################
        def mode_standby():
            '''
            Standby mode.
            '''
            #status = self.mpdclient.status()
            #DEBUG(status)
            self.alarmed = None
            timestr = time.strftime('%H:%M:%S')
            #self.mpd_connect()
            self.mpdclient.stop()
            display(f"    {timestr}   ", 1)
            display("    STANDBY    ", 2)



        #######################################################################################
        def check_alarms():
            '''
            This runs when the selected program is playing and
            checks if alarm has ended and stop play if so.
            '''
            if self.mode != self.modes.ALARM:
                return
            if self.alarmed is None:
                return
            #DEBUG("Checking alarms")
            try:
                status = self.mpdclient.status()
            except Exception as e:
                DEBUG(e)
                return
            item = self.alarm_list[self.alarmed]
            title = self.mpdclient.currentsong().get('title')
            if title:
                display(title, 2)
            else:
                display(f"{item['name']}",2)
            duration = dp.parse(item['duration'])
            #DEBUG(item)
            if datetime.now() > item['stop']:
                song = status.get('song') # Either None or the index
                INFO(song)
                INFO(self.alrm_idx)
                self.mpdclient.stop()
                # Remove it from the playlist.
                # It should be at the end so list remains coherent.
                try:self.mpdclient.delete(self.alrm_idx)
                except Exception as e: DEBUG(e)
                self.alarmed = None # Should force rebuild of alarm list but also needs...
                self.alarm_list = None
                self.alrm_idx = None
                self.mixer.setvolume(self.volume)
                INFO(f"Item {item.get('name')} stopped.")
                display(f"{item.get('name')} END", 2)
                #self.mpdclient.stop()

                # Restore last playing item set by tuner.
                try:
                    if self.mode == self.modes.PLAY_PLUS: # Alarm interrupted an already playing item so resume.
                        self.mpdclient.play(int(self.current_playing['idx'])) # Error Integer expected:
                except Exception as ex:
                    INFO(str(ex))
                    INFO("Cannot restore last playing.") # Because it may not exist yet.
                self.set_alarm()


        #######################################################################################
        def mode_alarm():
            '''
            Alarm Mode.
            This should allow playback to continue while waiting, unless the standby switch is also set.
            Use the web interface to set up alarms.
            '''
            timestr = time.strftime('%H:%M:%S')
            display(f"    {timestr}   ", 1)
            #display("    ALARMED   ", 2) # This needs more work
            if self.alarm_list == None or len(self.alarm_list) == 0:
                self.alarmed = None
                #INFO("Making alarm list")
                try:
                    self.mpdclient.stop()
                except:
                    self.mpd_connect()
                self.alarm_list = self.make_alarms()
            if self.alarmed == None:
                self.set_alarm()
            check_alarms()


        ############## WHILE TRUE LOOP ####################################################################
        while True:
            try:
                self.mpdclient.ping() # MPDClient will disconnect after period of inactivity, so keep it busy.
            except Exception as e:
                DEBUG(e)
                self.mpd_connect()
            #Check if the playlist file has been updated and reload if it has.
            if self.file_time != os.stat(self.play_file).st_mtime:
                INFO("Playlist file has changed, reloading...")
                self.read_playlist_file(self.play_file)
                self.file_time = os.stat(self.play_file).st_mtime
                self.playlist = self.read_playlist_file(self.play_file) # From disk
                self.load_playlist() # Into MPD
                self.main()
            if self.alarm_time != os.stat(self.alarm_file).st_mtime:
                INFO("Alarm file has changed, reloading...")
                self.alarm_time = os.stat(self.alarm_file).st_mtime
                self.alarm_list = self.make_alarms()
            # Check if the program itself has been updated.
            if self.this_time != os.stat(this).st_mtime: # The program has been updated.
                INFO("App has been updated, restarting.")
                self.this_time = os.stat(this).st_mtime
                self.restart()
            #INFO(self.mode)
            # Get the switch positions here.
            #self.switch_callback() # periodic call.
            if self.mode >= self.modes.TUNING_0 and self.mode <= self.modes.TUNING_2: # Tuning mode, set by knob button.
                mode_tuning() # Don't sleep, just loop again
            # Modes detect and call current functions in main()
            elif self.mode == self.modes.PLAY:
                mode_play()
            elif self.mode == self.modes.PLAY_PLUS:
                mode_play_plus()
            elif self.mode == self.modes.ALARM:
                mode_alarm() #
            elif self.mode == self.modes.STANDBY:
                mode_standby()
            #time.sleep(0.5)


########### END WHILE TRUE LOOP ##########################################################





######################################################################################
if __name__ == '__main__':
    INFO(f'STARTING "THE WIRELESS. VERSION:{version.__version__}"')
    radio = Radio()
    radio.main()
