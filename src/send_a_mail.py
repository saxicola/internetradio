# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.message import EmailMessage


def send_mail(message):


    # Create a text/plain message
    msg = EmailMessage()
    msg.set_content(message)

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = 'Radio says Hello'
    msg['From'] = 'Radio'
    msg['To'] = 'mikee@saxicola.co.uk'

    # Send the message via our own SMTP server.
    s = smtplib.SMTP('192.168.1.11')
    s.send_message(msg)
    s.quit()


if __name__ ==  '__main__':
    send_mail('Testing\n 1 2 3\n')
