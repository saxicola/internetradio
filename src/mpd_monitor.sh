#!/bin/bash
# Start this in a cron @reboot job.
# Loop and monitor port 8000 for a connection from the radio.
# If it connects then resume playing from MPD.
# When it disconnects pause playing.
# DONE Check the output of mpc for which file we are playing and loop if we have
# reached the end of the list.
# DONE Save the play position and reload it on boot so we don't have to start again.
# We need to parse: [playing] #25/116   0:09/31:06 (0%)
# for the track number and total tracks.
#
# e.g: echo "[playing] #25/116   0:09/31:06 (0%)" | awk -F'#' '{print $2 }' |awk -F'/' '{print $2 }'
# echo "[playing] #25/116   0:09/31:06 (0%)" | awk -F'#' '{print $2 }' |awk -F'/' '{print $1 }'
# THis needs a service file.








playing=0


if [ -t 1 ] ; then # This is being run from a terminal, probably for testing.
  echo Skipping startup stuff.
  logger Skipping startup stuff.
else # Probably run from cron so...
  mpc clear;
  mpc add /;
  mpc play; # Need to play else the ark stuff will return empty value.
  result=$(mpc pause);
  # It would be nice to start at a random position.
  tracks=$(echo $result | awk -F'#' '{print $2 }' | awk -F'/' '{print $2 }' | awk '{print $1}'); # Gets total number of tracks
  rand=$(echo $((1 + RANDOM % $tracks)));
  source lastplay # On reboot start from here not at 001. From $HOME/lastplay
  mpc play $rand # Move to the random track then...
  #mpc play $lastplay # On reboot start from here not at 001
  mpc pause # wait!
fi


while [ 1 ]
do
  # netstat (with some grepping) will tell us when the radio connects to the server on port 8000
  stat=$(netstat -tan | grep 8000 | grep ESTABLISHED) # -t=tcp, -a=all, -n=numeric
  if [[ -n $stat  ]];
  then
      if [[ $playing == 0 ]]
      then
        echo Radio has connected.
        logger Radio has connected to MPD.
        playing=1
        result=$(mpc play) # From the last/current position
        track=$(echo $result | awk -F'#' '{print $2 }' | awk -F'/' '{print $1 }' )
        tracks=$(echo $result | awk -F'#' '{print $2 }' | awk -F'/' '{print $2 }' | awk '{print $1}')
        echo Playing track $track of $tracks
      fi
  else
    if [[ $playing == 1 ]]
    then
      echo radio has disconnected.
      logger Radio has disconnected to MPD.
      playing=0
      mpc seek 0 # Go back to the beginning of the track.
      result=$(mpc pause)
      track=$(echo $result | awk -F'#' '{print $2 }' | awk -F'/' '{print $1 }' ) # Save this
      echo $track # DEBUG
      echo $track > lastplay # Output goes it $HOME
    fi
  fi
  sleep 1
done
