#!/usr/bin/env python

import os
import time
import RPi.GPIO as GPIO
import signal
from subprocess import check_output
import sys
import syslog
# tell the GPIO module that we want to use
# the chip's pin numbering scheme

GPIO.setmode(GPIO.BCM)


# How long to wait, in seconds
POWER_WAIT = 30

# Pin definitions
BUTTON = 11 # Oooh, what can we use this for?
POWER_LOSS = 22
RELAY = 27
p = None
t_shutdown = None


# handle the power change event
def signal_power_change_handler(pin):
    global p
    global t_shutdown
    syslog.syslog("UPS: Power lost")
    #p.start(50) # Power saving, 50%, for relay hold on
    if GPIO.input(22) == 1 and not t_shutdown:
        syslog.syslog("UPS: Power lost. Shutting down in {} seconds.".format(POWER_WAIT,))
        t_shutdown = time.time() + POWER_WAIT
        # Loop to check if the power is still off
        while time.time() < t_shutdown: # Loop for POWER_WAIT seconds
            time.sleep(1)
        if GPIO.input(22) == 1: # Check the pin state again
            # If it's still high, order a shut down
            #os.system("sudo pkill omxplayer")
            #syslog.syslog("UPS: video player killed")
            #os.system("sudo systemctl stop apache2")
            #sys.syslog("UPS: apache2 killed")
            syslog.syslog("UPS: Ordered power down")
            os.system("sudo shutdown -h now")
            GPIO,cleanup()
            quit(0)
        else:
            t_shutdown = None
            #p.stop() # Back to just ON
            GPIO.output(27,True)
            syslog.syslog("UPS: Power restored")
            return


def sig_pin_high():
    return


def sig_pin_low():
    return


def signal_handler(sig, frame):
    syslog.syslog("UPS: Killed by CTRL-C")
    GPIO.cleanup()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)


# main function
def main():
    global p
    global vid_pid
    GPIO.cleanup() # Any old stuff.
    timestr = time.strftime("%Y-%m-%d %H:%M:%S")
    syslog.syslog("UPS: System up")
    # Wait a bit for power to settle down.
    time.sleep(5) # seconds
    GPIO.setup(22,GPIO.IN) # Only on > Rev3 boards
    GPIO.setup(BUTTON, GPIO.IN)
    GPIO.setup(27,GPIO.OUT)
    # tell the GPIO library to look out for an
    # event on pin 15 and deal with it by calling
    # the handler function
    GPIO.remove_event_detect(22) # remove any previous event triggers
    GPIO.add_event_detect(22,GPIO.BOTH, callback=signal_power_change_handler, bouncetime=30)
    # turn ON the UPS control
    GPIO.output(27,True)
    while 1:
        time.sleep(1)
    GPIO.cleanup()

if __name__=="__main__":
    main()
