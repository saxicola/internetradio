#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Note there is no security here and anyone can do anything.

import copy
from flask import Flask, request, render_template
import json
import logging
#from m3u_parser import M3uParser
#from mpd import MPDClient
import os
import subprocess

logging.basicConfig(level=logging.DEBUG,format='%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
DEBUG = logging.debug
INFO = logging.info

logging.disable(logging.DEBUG)
#logging.disable(logging.INFO)

here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

app = Flask(__name__)

# Globals. I hate globals, however...
playlist = []
alarmlist = []
deleted = False


'''
Alarm setup page
Same as old stations page.
'''
@app.route('/alarms')
def alarms_setup():
    global alarmlist
    alarmlist = load_playlist("alarms.json")
    # Open the existing alarms list
    if request.method != 'POST':
        alarmlist = load_playlist("alarms.json")
        return render_template('alarms.html', play_list = alarmlist)
    return render_template('alarms.html', play_list = alarmlist)


@app.route('/alarms', methods=['POST'])
def alarms_setup_post():
    global deleted
    global alarmlist
    deleted = False
    #alarmlist = load_playlist("alarms.json")
    #TODO
    for k,v in request.form.items():
        print (k, v)
        # Get the checked boxes list. Need them in reverse order because if we remove from the
        # beginning the list get re-indexed and the indexes don't match anymore.
        # If we remove from the end first then re-indexing doesn't matter.
    checks = sorted(request.form.getlist('checks'), reverse=True)
    INFO(len(checks))
    # Remove them. Remember to subtract 1 though
    if len(checks) > 0:
        for item, idx in reversed(list(enumerate(checks))):
            alarmlist.pop(int(idx)-1)
            INFO(f"Item {idx} removed.")
        deleted = True

    try: # If save button pressed this runs.
        save = request.form['save'].strip()
        save_playlist(alarmlist, "alarms.json")
        INFO("Saving to disk.")
        return alarms_setup()
    except KeyError as e: # Otherwise this does.
        try: # This will fail if just loading the URL fresh
            submit = request.form['edit'].strip()
        except KeyError as e:
            INFO("Not a post request")
            return my_form()
        url = request.form['URL'].strip()
        t_start = request.form['t_start'].strip()
        d_start = request.form['d_start'].strip()
        #t_end   = request.form['t_end'].strip()
        d_end   = request.form['d_end'].strip()
        name    = request.form['name'].strip()
        genre   =  request.form['genre'].strip()
        try:
            duration = request.form['duration'].strip()
        except:
            duration = "00:00"
        try:
            weekday    =  request.form['weekday'].strip()
            INFO(weekday)
        except: weekday = ''
        try:
            comment   =  request.form['comment'].strip()
        except:
            comment = ""
        if(t_start) == '': t_start = None
        if(d_start) == '': d_start = None
        if(d_end) == '': d_end = None

        if url != '':
            item = {'name':name, 'url':url, 't_start':t_start,  "duration":duration, \
                'd_start':d_start, 'd_end':d_end, 'comment':comment, 'genre':genre, 'weekday':weekday}
            alarmlist.append(item)
            INFO(f"added {item} to playlist.")
    return render_template('alarms.html', play_list = alarmlist)


'''
A Wifi Setup Page
Which can't be accessed until the WiFi is setup? Ha!
OK plug in a cable...
'''
@app.route('/wifi-setup')
def wifi_setup():
    return render_template('wifi_setup.html')


@app.route('/wifi-setup', methods=['POST'])
def wifi_setup_post():
    # TODO write to /etc/wpa_supplicant/wpa_supplicant.conf
    wpa_file = '/etc/wpa_supplicant/wpa_supplicant.conf'
    header = """ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
country=GB
update_config=1
    """
    #INFO(header)
    wifi_ap = request.form['ap'].strip()
    wifi_passwd = request.form['passwd'].strip()
    #INFO(wifi_passwd)
    if  wifi_ap != '' and  wifi_passwd != '':
        output = subprocess.check_output(f"wpa_passphrase {wifi_ap} {wifi_passwd}", shell=True)
    else:
        return render_template('wifi_setup.html')
    #print(f"wps_supplicant: {output}")
    # TODO Write the file
    wpa_file = "wpa_supplicant.conf" # Test file
    with open(wpa_file, 'w') as wpf:
        wpf.write(header + output.decode("utf-8") )
    # Restart WiFi
    #subprocess.check_output("service networking restart")
    # Check the WiFi status.
    status = subprocess.check_output("iwconfig")
    #INFO(status)
    # status should contain and AP string we entered.
    if wifi_ap  in status.decode("utf-8"):
        #INFO("Happy WIFI")
        result = ":)"
    else:
        #INFO("Sad WIFI")
        result = ":("
    # Hopefully we can login again via WiFi.
    return render_template('wifi_setup.html', message = result)





def load_playlist(playlist="playlist.json"):
    '''
    '''
    with open(os.path.join(here,playlist),'r') as plf: # But what if there is no playlist?
        playlist = json.loads(plf.read())
    #print(playlist)
    return playlist


def save_playlist(thelist, listfile="playlist.json"):
    with open(os.path.join(here, listfile), 'w') as fp:
        json.dump(thelist, fp, indent=2)
    #print(playlist)
    #return playlist


@app.route('/')
def my_form():
    global deleted
    global playlist
    # Open the existing play list
    if request.method != 'POST':
        playlist = load_playlist("stations.json")
        playlist.sort(key = lambda x: (x is None, x['name']))
        return render_template('my_form.html', play_list = playlist)
    else:
        INFO(len(playlist))
        if len(playlist) == 0: # or deleted == False:
            INFO("Loading new playlist")
            playlist = load_playlist("stations.json")
            playlist.sort(key = lambda x: (x is None, x['name']))
        return render_template('my_form.html', play_list = playlist)
        deleted = False


@app.route('/', methods=['POST'])
def my_form_post():
    #if request.method == 'POST': # SHould be here. TODO
    global deleted
    global playlist
    #playlist = load_playlist("stations.json")
    deleted = False
    for k,v in request.form.items():
        print (k, v)
        # Get the checked boxes list. Need them in reverse order because if we remove from the
        # beginning the list get re-indexed and the indexes don't match anymore.
        # If we remove from the end first then re-indexing doesn't matter.
    checks = sorted(request.form.getlist('checks'), reverse=True)
    INFO(len(checks))
    # Remove them. Remember to subtract 1 though
    if len(checks) > 0:
        for idx in checks:
            playlist.pop(int(idx)-1)
            INFO(f"Item {idx} removed.")
        deleted = True

    try: # If save button pressed this runs.
        save = request.form['save'].strip()
        save_playlist(playlist, "stations.json")
        INFO("Saving to disk.")
        return my_form()
    except KeyError as e: # Otherwise this does.
        try: # This will fail if just loading the URL fresh
            submit = request.form['edit'].strip()
        except KeyError as e:
            INFO("Not a post request")
            return my_form()
        url = request.form['URL'].strip()
        name    = request.form['name'].strip()
        genre   =  request.form['genre'].strip()
        try:
            comment   =  request.form['comment'].strip()
        except:
            comment = ""
        if url != '':
            item = {'name':name, 'url':url, 'comment':comment, 'genre':genre}
            playlist.append(item)
            INFO(f"added {item} to playlist.")
    return my_form()


if __name__ == '__main__':
    app.run(debug=True, port=80, host='0.0.0.0')
